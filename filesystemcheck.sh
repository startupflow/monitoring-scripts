#!/bin/bash
FILE="machines-list"
COMMANDS="commands.txt"
RED='\033[0;31m'
NC='\033[0m'
LIGHTPURPLE='\33[1;35m'
CYAN='\033[0;36m' 
LIGHTBLUE='\33[1;34m'
GREEN='\033[0;32m'
STARTTIME=`date +%s`
if [ ! -f stats_template.sh ]; then
	echo "Stats template doesnt exist, creating one..."
	touch stats_template.sh
elif [[ -s stats_template.sh ]]; then
	echo "Clearing stats_template.sh"
	> stats_template.sh
else
	echo "File is empty collectuing data. "
fi
	echo "printf \"${CYAN}---------------------------------------------------------------------------------------------------------------------${NC}\n\"" >> stats_template.sh
	
	echo "printf \"${CYAN}|${NC}${LIGHTPURPLE}%-10s${NC}${CYAN}|${NC}${LIGHTPURPLE}%-10s${NC}${CYAN}|${NC}${LIGHTPURPLE}%-5s${NC}${CYAN}|${NC}${LIGHTPURPLE}%-20s${NC}${CYAN}|${NC}${LIGHTPURPLE}%-20s${NC}${CYAN}|${NC}${LIGHTPURPLE}%-45s${NC}${CYAN}|${NC}\n\" \"SERVER\" \"STATUS\" \"CPU\" \"RAM\" \"DISK\" \"UPTIME\" " >> stats_template.sh
	
	echo "printf \"${CYAN}---------------------------------------------------------------------------------------------------------------------${NC}\n\"" >> stats_template.sh
while IFS='' read MACHINE || [[ -n "$MACHINE" ]]; do
	STATUS=$(ssh -n -q -o "BatchMode=yes" -o "ConnectTimeout=10" sf@${MACHINE} echo ok 2>&1)	
	if [[ ${STATUS} == ok ]]; then
	    echo "${MACHINE} is up getting data..."
	    
	    declare status="UP"
	      
	    
	    export cpu="$(ssh -n sf@$MACHINE "top -b -d1 -n1|grep -i \"CPU(s)\"|head -c21|cut -d \" \" -f3|cut -d \"%\" -f1")"
	       

	    export ram="$(ssh -n sf@${MACHINE} "free -m | awk 'NR==2{printf \"%s/%sMB %.f%%\", \$3,\$2,\$3*100/\$2 }'")"
	        

	    export disk="$(ssh -n sf@${MACHINE} "df -h | awk '\$NF==\"/\"{printf \"%d/%dGB %.f%%\", \$3,\$2,\$5}'")"
	        
	
	    export uptime="$(ssh -n sf@${MACHINE} "uptime -p")"
 	        		
	    echo "printf \"${CYAN}|${NC}%-10s${CYAN}|${NC}${GREEN}%-10s${NC}${CYAN}|${NC}%-5s${CYAN}|${NC}%-20s${CYAN}|${NC}%-20s${CYAN}|${NC}%-45s${CYAN}|${NC}\n\" \"${MACHINE}\" \"${status}\" \"${cpu}\" \"${ram}\" \"${disk}\" \"$uptime\" " >> stats_template.sh
	    echo "printf \"${CYAN}---------------------------------------------------------------------------------------------------------------------\n${NC}\n\"" >> stats_template.sh
		

	elif [[ ${STATUS} == "Permission denied"* ]]; then
		echo "Permission denied for ${MACHINE}"
	else
	    echo "${MACHINE} is down..."
	    declare status="DOWN"
	    
	    echo "printf \"${CYAN}|${NC}${RED}%-10s${NC}${CYAN}|${NC}${RED}%-10s${CYAN}|${NC}%-93s${CYAN}|${NC}\n\" \"${MACHINE}\" \"${status}\"" >> stats_template.sh
	    echo "printf \"${CYAN}---------------------------------------------------------------------------------------------------------------------${NC}\n\"" >> stats_template.sh
	fi

done < $FILE
echo 'creating variables finished'
ENDTIME=`date +%s`
EXECUTIONTIME=$(((ENDTIME - STARTTIME)/60))
echo "Script execution time ${EXECUTIONTIME}"
bash ./stats_template.sh
