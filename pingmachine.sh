#!/bin/bash
FILE="machines-list"
start=`date +%s`
while read MACHINE; do
	STATUS=$(ssh -n -q -o "BatchMode=yes" -o ConnectTimeout=10 sf@${MACHINE} echo ok 2>&1)
	#STATUS=$(ssh -q -o "BatchMode=yes" $MACHINE "echo 2>&1" && echo $host SSH_OK || echo $host SSH_NOK)
	if [[ ${STATUS} == ok ]]; then
		echo " ${MACHINE} UP"
	elif [[ ${STATUS} == "Permission denied" ]]; then
	        echo "Permission denied for ${MACHINE}"
				 
	else
		echo " ${MACHINE} DOWN"
	fi
done < ${FILE}
end=`date +%s`
runtime=$((end-start))
echo "Execution time ${runtime}"
