#!/bin/bash
FILE="machines-list"

while IFS='' read -r MACHINE || [[ -n "$MACHINE" ]]; do
	echo "Transfering SSH key to: $MACHINE"
	ssh-copy-id -i ~/.ssh/id_rsa.pub $MACHINE
done < $FILE	
